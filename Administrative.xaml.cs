﻿using Microsoft.Win32;
using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Tema1
{
    /// <summary>
    /// Interaction logic for Administrative.xaml
    /// </summary>
    public partial class Administrative : Window
    {
        public Administrative()
        {
            InitializeComponent();
            this.Top = 10;
            this.Left = 300;
            
            if (new FileInfo("D:\\FACULTATE\\anul 2\\Sem II\\MVP\\Proiecte\\Tema1\\dex.txt").Length==0)
            {
                txtid.Text = "1";
            }
            else
            {
                List<string> comboBox = new List<string>();
                comboBox = (DataContext as Dictionary).Read();
                int i = 0;
                while (i < comboBox.Count)
                {
                    if (txtcategory.Items.Contains(comboBox[i]) == false)
                    {
                        txtcategory.Items.Add(comboBox[i]);
                    }
                    i++;
                }
                string lastLine = File.ReadLines("D:\\FACULTATE\\anul 2\\Sem II\\MVP\\Proiecte\\Tema1\\dex.txt").Last();
                string[] textLine;
                textLine = lastLine.Split('|');

                txtid.Text = textLine[0];
                int index = int.Parse(txtid.Text);
                index += 1;
                txtid.Text = index.ToString();
            }
            string path = "D:\\FACULTATE\\anul 2\\Sem II\\MVP\\Proiecte\\Tema1\\dex.txt";
            File.OpenWrite(path);

        }


        int id;
        bool firstClick = true;
        string category = null;

        string imgPath = null;
        private void addWords_Click(object sender, RoutedEventArgs e)
        {
            if(firstClick==true)
            {
                firstClick = false;
                id = int.Parse(txtid.Text);
            }

            string path = "D:\\FACULTATE\\anul 2\\Sem II\\MVP\\Proiecte\\Tema1\\dex.txt";

            if (imgPath == null)
            {
                imgPath = "C:/Users/User/OneDrive/Documents/noImage.jpg";
            }

            string txt = txtid.Text +"|"+ txtword.Text + "|" + txtcategory.Text + "|" + txtdescription.Text + "|" + imgPath;
            
            using (StreamWriter objWriter = new StreamWriter(path, true))
            {
                objWriter.WriteLine(txt);
            }
            
            (DataContext as Dictionary).DictionaryItems.Add(new Word()
            {
                Id=id.ToString(),
                WordName = txtword.Text,
                Category = txtcategory.Text,
                Description = txtdescription.Text,
                Path = imgPath
            }) ;
            image.Source = new BitmapImage(new Uri("C:/Users/User/OneDrive/Documents/noImage.jpg"));
            imgPath = null;
            id++;
            txtid.Text = id.ToString();

            txtNewCategory.Clear();
            txtdescription.Clear();
            txtword.Clear();
            txtcategory.SelectedIndex = -1;

            if(AddCategory.IsChecked==true)
            {
                AddCategory.IsChecked = false;
            }
        }

        private void AddCategory_Checked(object sender, RoutedEventArgs e)
        {
            category = txtNewCategory.Text;
            txtcategory.Items.Add(category);
        }

        private void modifyWords_Click(object sender, RoutedEventArgs e)
        {
            imgPath = image.Source.ToString();
            string txt = txtid.Text + "|" + txtword.Text + "|" + txtcategory.Text + "|" + txtdescription.Text + "|" + imgPath;

            var item = (DataContext as Dictionary).DictionaryItems.FirstOrDefault(x => x.Id == txtid.Text);
            int i = (DataContext as Dictionary).DictionaryItems.IndexOf(item);

            (DataContext as Dictionary).DictionaryItems[i] = new Word()
            {
                Id = txtid.Text,
                WordName = txtword.Text,
                Description = txtdescription.Text,
                Category = txtcategory.Text,
                Path = image.Source.ToString()
            };

            Read read=new Read();
            read.ChangeFile(txtid.Text, txt);

            string lastLine = File.ReadLines("D:\\FACULTATE\\anul 2\\Sem II\\MVP\\Proiecte\\Tema1\\dex.txt").Last();
            string[] textLine;
            textLine = lastLine.Split('|');

            txtid.Text = textLine[0];
            int index = int.Parse(txtid.Text);
            index += 1;

            txtid.Text = index.ToString();

            txtNewCategory.Clear();
            txtdescription.Clear();
            txtword.Clear();
            txtcategory.SelectedIndex = -1;

            if (AddCategory.IsChecked == true)
            {
                AddCategory.IsChecked = false;
            }
        }
       
        private void deleteWords_Click(object sender, RoutedEventArgs e)
        {
            imgPath = image.Source.ToString();
            string txt = txtid.Text + "|" + txtword.Text + "|" + txtcategory.Text + "|" + txtdescription.Text + "|" + imgPath;

            (DataContext as Dictionary).DictionaryItems.Remove(
                (DataContext as Dictionary).DictionaryItems.Where(x => x.Id == txtid.Text).Single());
            Read read = new Read();
            read.DeleteItem(txtid.Text);


            txtNewCategory.Clear();
            txtdescription.Clear();
            txtword.Clear();
            txtcategory.SelectedIndex = -1;

            //
            txtcategory.Items.Clear();
            //window initialization
            List<string> comboBox = new List<string>();
            (DataContext as Dictionary).DictionaryItems.Clear();
            comboBox = (DataContext as Dictionary).Read();
            
            int i = 0;
            while (i < comboBox.Count)
            {
                txtcategory.Items.Add(comboBox[i]);
                i++;
            }
            string lastLine = File.ReadLines("D:\\FACULTATE\\anul 2\\Sem II\\MVP\\Proiecte\\Tema1\\dex.txt").Last();
            string[] textLine;
            textLine = lastLine.Split('|');

            txtid.Text = textLine[0];
            int index = int.Parse(txtid.Text);
            index += 1;
            txtid.Text = index.ToString();
            //

            image.Source = new BitmapImage(new Uri("C:/Users/User/OneDrive/Documents/noImage.jpg"));
            imgPath = null;
        }

        private void addImg_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new CommonOpenFileDialog();
            dialog.IsFolderPicker = false;
            CommonFileDialogResult result = dialog.ShowDialog();
            imgPath = dialog.FileName;

            var uriSource = new Uri(imgPath);
            image.Source = new BitmapImage(uriSource);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }

        private void lblId_SelectedIndexChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lblId.SelectedIndex != -1)
            {
                txtid.Text = (lblId.SelectedItem as Word).Id;
                txtword.Text = (lblId.SelectedItem as Word).WordName;
                txtcategory.Text = (lblId.SelectedItem as Word).Category;
                txtdescription.Text = (lblId.SelectedItem as Word).Description;
                image.Source = new BitmapImage(new Uri((lblId.SelectedItem as Word).Path));
            }
        }
    }
}
