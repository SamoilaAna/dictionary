﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
//using WPFTextBoxAutoComplete;
namespace Tema1
{
    /// <summary>
    /// Interaction logic for Search.xaml
    /// </summary>
    public partial class Search : Window
    {
        List<string> wordsListBox;
        public Search()
        {
            InitializeComponent();
            wordsListBox = new List<string>();
            string pathToRead = "D:\\FACULTATE\\anul 2\\Sem II\\MVP\\Proiecte\\Tema1\\dex.txt";
           
            string[] Line = File.ReadAllLines(pathToRead);
            int m = 0;
            while(m<Line.Length)
            {
                string[] str;
                str = Line[m].Split('|');
                wordsListBox.Add(str[1]);
                m++;
            }

            txtBox.TextChanged += new TextChangedEventHandler(txtBox_TextChanged);
            
        }

        private void txtBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            string strWord = txtBox.Text;
            List<string> listWords = new List<string>();

            foreach(string s in wordsListBox)
            {
                if(!string.IsNullOrEmpty(txtBox.Text))
                {
                    if(s.StartsWith(strWord))
                    {
                        listWords.Add(s);
                    }
                }
            }
            if(listWords.Count>0)
            {
                list.ItemsSource = listWords;
                list.Visibility = Visibility.Visible;
            }
            else
            {
                if(txtBox.Text.Equals(""))
                {
                    list.Visibility = Visibility.Collapsed;
                    list.ItemsSource = null;
                }
                else
                {
                    list.Visibility = Visibility.Collapsed;
                    list.ItemsSource = null;
                }
            }
        }

        private void list_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(list.ItemsSource!=null)
            {
                list.Visibility = Visibility.Collapsed;
                txtBox.TextChanged -= new TextChangedEventHandler(txtBox_TextChanged);
                if(list.SelectedIndex != -1)
                {
                    txtBox.Text = list.SelectedItem.ToString();
                }
                txtBox.TextChanged += new TextChangedEventHandler(txtBox_TextChanged);
            }
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            string fileName = "D:\\FACULTATE\\anul 2\\Sem II\\MVP\\Proiecte\\Tema1\\dex.txt";

            string[] arrLine = File.ReadAllLines(fileName);
            int index = 0;
            while (index < arrLine.Length)
            {
                string[] textLine;
                textLine = arrLine[index].Split('|');

                if (textLine[1] == txtBox.Text)
                {
                    word.Text = textLine[1];
                    category.Text = textLine[2];
                    description.Text = textLine[3];
                    image.Source = new BitmapImage(new Uri(textLine[4]));

                    break;
                }
                index++;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }
    }
}
